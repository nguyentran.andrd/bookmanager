package com.example.demo.controller;

import com.example.demo.models.Student;
import com.example.demo.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {


    @Autowired
    StudentRepository stRepository;

    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer one,
                @RequestParam("b") Integer two) {
        return one + two;
    }

    @PostMapping("/create-student")
    void createAuthor(@RequestBody Student student) {
        stRepository.save(student);
    }
}
